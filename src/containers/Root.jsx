import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { Landing } from '../components'

class Root extends Component {
  constructor (props) {
    super(props)
  }
  render () {
    return <Landing />
  }
}

const mapStateToProps = state => ({
  app: state.app
})
// const mapDispatchToProps = dispatch => ({
//   couponActions: bindActionCreators(couponsActions, dispatch),
// })

export default connect(mapStateToProps)(Root)
