import React, { Component, Fragment } from 'react'
import { PageWrapper } from '../'
import { List } from '../items'
import '../../styles/pages/TokenPresale.scss'

const items = [
  'Decent passive earning on professional <br /> trading of the best crypto traders',
  'All the crypto is kept in the wallet of investor at all <br /> time',
  'Success fees only'
]

const TokenPresale = ({ clickNext, fadein = true }) => (
  <PageWrapper page={0} id='tokenPresale'>
    <div className={`TP_layout ${fadein && 'fadein'}`}>
      <div className='page'>
        <h1>Token pre-sale</h1>
        <List items={items} />
        <div className='block_inline centered_h'>
          <button>join pre-sale</button>
          <button className='transparent'>test mvp</button>
          <input type='checkbox' id='terms'/><label style={{marginLeft: 30}}for='terms'>I agree with the processing <br />of my personal data</label>
        </div>
      </div>
    </div>
    <button className={`down ${fadein && 'fadein'}`} onClick={clickNext}></button>
  </PageWrapper>
)

export default TokenPresale
