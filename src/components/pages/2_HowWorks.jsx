import React, { Component, Fragment } from 'react'
import { PageWrapper } from '../'
import '../../styles/pages/HowWorks.scss'

const HowWorks = ({ clickNext }) => (
  <PageWrapper page={1} id='howWorks'>
    <div className='HW_layout'>
      <div className='info'>
        <h1>How the<br />technology<br />works for<br/>investor</h1>
        <button>watch video</button>
      </div>
      <div className='video'></div>
      <button className='down' onClick={clickNext}></button>
    </div>
  </PageWrapper>
)

export default HowWorks
