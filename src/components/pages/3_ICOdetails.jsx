import React, { Component, Fragment } from 'react'
import { PageWrapper } from '../'
import '../../styles/pages/ICOdetails.scss'

const ICOdetails = ({ clickNext }) => (
  <PageWrapper page={2} id='icoDetails'>
    <div className='ID_layout'>
      <h1>ICO details</h1>
      <p>Token name: <span>CINDEX</span></p>
      <p>ICO: <span>16.05.2018 – DD.MM.2018</span></p>
      <p>Public supply: <span>80.000.000</span></p>
      <p>Price per token: <span>$ 1.00</span></p>
      <p>Minimal goal: <span>$ 6.000.000</span></p>
      <button>buy tokens</button>
      <button className='down' onClick={clickNext}></button>
    </div>
  </PageWrapper>
)

export default ICOdetails
