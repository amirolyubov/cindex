import React, { Component, Fragment } from 'react'
import Parallax from 'react-springy-parallax'
import '../styles/Landing.scss'

class PageWrapper extends Component {
  constructor (props) {
    super(props)
    this.state = {
      positionFromTop: 0
    }
  }
  componentDidMount () {
    const { id } = this.props
    this.setState({ positionFromTop: document.getElementById(id).getBoundingClientRect().top })
  }
  render () {
    const { children, page, parallax, id } = this.props
    return (
      <div className='page_wrapper' id={id}>
        { children }
      </div>
    )
  }
}

export default PageWrapper
