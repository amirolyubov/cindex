import React, { Component, Fragment } from 'react'
import Parallax from 'react-springy-parallax'
import Animated from 'animated/lib/targets/react-dom'
import Easing from 'animated/lib/Easing'
import '../styles/Landing.scss'
import '../styles/_common.scss'
import '../styles/backgrounds.scss'
import { Nav, PageWrapper } from './'
import {
  TokenPresale,
  HowWorks,
  ICOdetails
} from './pages'

class Landing extends Component {
  constructor (props) {
    super(props)
  }
  handleClick = (p, secondP, to) => {
    p.scrollTo(to)
    secondP.scrollTo(to)
  }
  render () {
    console.log(this);
    return (
      <Fragment>
        <Nav />
        <Parallax
          ref={ref => this.secondParallax = ref}
          pages={4}
          scrolling={false}>
              <Parallax.Layer
                speed={-1.1}
                offset={0}
                >
                <div className='mountain'></div>
              </Parallax.Layer>
        </Parallax>
        <Parallax
          ref={ref => this.parallax = ref}
          pages={4}
          scrolling={false}
          effect={(animation, toValue) =>
              Animated.timing(animation, { toValue, duration: 0, easing: Easing.linear })}>
          <div className='app_layout'>
            <div className='content'>
              <Parallax.Layer offset={0} speed={0}>
                <TokenPresale clickNext={() => this.handleClick(this.parallax, this.secondParallax, 1)}/>
              </Parallax.Layer>
              <Parallax.Layer offset={1} speed={0}>
                <HowWorks clickNext={() => this.parallax.scrollTo(2)} />
              </Parallax.Layer>
              <Parallax.Layer offset={2} speed={0}>
                <ICOdetails clickNext={() => this.parallax.scrollTo(0)} />
              </Parallax.Layer>
            </div>
          </div>
        </Parallax>
      </Fragment>
    )
  }
}

export default Landing
