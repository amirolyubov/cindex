import React, { Component, Fragment } from 'react'
import { PageWrapper } from '../'
import '../../styles/pages/HowWorks.scss'

const List = ({ items }) => (
  <ul>
    {
      items.map((item, key) => item.color
                               ? <li key={key}><p dangerouslySetInnerHTML={{__html: item.text}}></p></li>
                               : <li key={key}><p dangerouslySetInnerHTML={{__html: item}}></p></li>)
    }
  </ul>
)

export default List
