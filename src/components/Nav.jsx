import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import '../styles/Nav.scss'

const Nav = () => (
  <header>
    <div className='left'>
      <div className='logo'></div>
      <div className='hook'>
        <p>Win on winners.</p>
        <p>Automatically.</p>
      </div>
    </div>
    <div className='right'>
      <Link to='/'>About</Link>
      <Link to='/'>FAQ</Link>
      <Link to='/'>Join sale</Link>
    </div>
  </header>
)

export default Nav
