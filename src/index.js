import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter } from 'react-router-dom'
import { Provider } from 'react-redux'
import { createStore, applyMiddleware, combineReducers } from 'redux'
import thunk from 'redux-thunk'

import reducer from './reducers'
import { Root } from './containers'

const store = createStore(combineReducers(reducer), applyMiddleware(thunk))

ReactDOM.render(
<Provider store={store}>
  <BrowserRouter>
    <Root />
  </BrowserRouter>
</Provider>,
document.getElementById('app_layout'))
